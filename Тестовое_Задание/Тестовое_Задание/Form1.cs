﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace Тестовое_Задание
{
    public partial class Form1 : Form
    {
        //Поля
        //Стартовая директория
        public string start_directory { get; set; }
        //Маска файла
        public string filename { get; set; }
        //Содержание файла
        public string text { get; set; }
        public Form1()
        {
            InitializeComponent();
            label_project_name.Text = AppDomain.CurrentDomain.FriendlyName;
            Get_Config();
        }
      
        //Загрузка данных
        private void Get_Config()
        {
            start_directory = Properties.Settings.Default.start_directory;
            filename = Properties.Settings.Default.filename;
            text = Properties.Settings.Default.text;
            textBox_filename.Text = filename;
            textBox_filetext.Text = text;
            textBox_start_directory.Text = start_directory;
        }
        //Сохранение данных
        public void Set_Config()
        {
            Properties.Settings.Default.text = text;
            Properties.Settings.Default.start_directory = start_directory;
            Properties.Settings.Default.filename = filename;
            Properties.Settings.Default.Save();
        }
        //Закрытие приложения
        private void button1_Click(object sender, EventArgs e)
        {
            Set_Config();
            if (t != null) t.Abort();
            this.Close();
        }
        //Выбор директории
        private void button_choose_directory_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                textBox_start_directory.Text = folderDlg.SelectedPath;
                start_directory = folderDlg.SelectedPath;
                Environment.SpecialFolder root = folderDlg.RootFolder;
            }
        }
        //Проверка на введённую вручную директорию
        private void textBox_start_directory_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox_start_directory.Text))
            {
                if (!Directory.Exists(textBox_start_directory.Text))
                {
                    MessageBox.Show("Указанная директория не существует","Ошибка");
                    ActiveControl = textBox_start_directory;
                }
                else start_directory = textBox_start_directory.Text;
            }
        }
        //Маска файла
        private void button_filename_Click(object sender, EventArgs e)
        {
            //Отдельная форма для создания маски
            filename_creator file_cr = new filename_creator(textBox_filename);
            file_cr.Show();
        }
       public ManualResetEvent manualResetEvent = new ManualResetEvent(true);
        Thread t;
        //Запуск поиска
        private void button_start_Click(object sender, EventArgs e)
        {
            label_project_name.Text = AppDomain.CurrentDomain.FriendlyName;
            //Если поток был инициализирован
            if (t!=null)
            {
                //Если находится в ожидании
                if(t.ThreadState==ThreadState.WaitSleepJoin)
                {
                    //Возобновляем процесс
                    manualResetEvent.Set();
                }
                else
                {
                    //Запускаем заново
                    if (!String.IsNullOrEmpty(start_directory))
                    {
                        New_Thread();
                        manualResetEvent.Set();
                        treeView_result.Nodes.Clear();

                    }
                    else MessageBox.Show("Укажите начальную директорию", "Ошибка");
                    
                }
            }
            //Иначе создание потока
            else
            {
                if (!String.IsNullOrEmpty(start_directory)) New_Thread();
                else MessageBox.Show("Укажите начальную директорию", "Ошибка");
            }
            button_start.Enabled = false;
            button_pause.Enabled = true;
            button_stop.Enabled = true;
        }
        private void New_Thread()
        {
            t = new Thread(Start);
            t.Start();
        }
        //Метод, который выполняется в отдельном потоке
        private void Start()
        {
            if (!String.IsNullOrEmpty(start_directory) || !String.IsNullOrEmpty(filename) || !String.IsNullOrEmpty(text))
            {
                Action action = () =>
                {
                    label_project_name.Text += " | Поиск";
                    if (!String.IsNullOrEmpty(start_directory)) label_project_name.Text += " с указ. директории,";
                    if (!String.IsNullOrEmpty(filename)) label_project_name.Text += " по шаблону файла,";
                    if (!String.IsNullOrEmpty(text)) label_project_name.Text += " по содержимому файла,";
                    label_project_name.Text = label_project_name.Text.Remove(label_project_name.Text.Length - 1, 1);
                    Timer_Tick = 0;
                    timer1.Start();
                };
                //Выполняем в основном потоке
                MyInvoke(action);
                Searcher searcher = new Searcher(this);
                button_start.Enabled = true;
                button_pause.Enabled = false;
                button_start.Text = "Запуск";
            }

        }
        //Считывание новой маски
        private void textBox_filename_TextChanged(object sender, EventArgs e)
        {
            filename = textBox_filename.Text;
        }
        //Считывание нового текста
        private void textBox_filetext_Leave(object sender, EventArgs e)
        {
            text = textBox_filetext.Text;
        }
        //Перемещение формы
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        private void label_project_name_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        private void panel_osn_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
      //Время поиска
      public  int Timer_Tick=0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            Timer_Tick++;
        }
        //Пауза поиска
        private void button_pause_Click(object sender, EventArgs e)
        {
            button_pause.Enabled = false;
            manualResetEvent.Reset();
            button_start.Enabled = true;
            button_start.Text = "Продолжить";
            
        }
        //Завершение поиска
        private void button_stop_Click(object sender, EventArgs e)
        {
            t.Abort();
            label_status.Text = "Статус: прервано пользователем";
            button_start.Enabled = true;
            button_pause.Enabled = false;
            button_stop.Enabled = false;
            button_start.Text = "Запуск";
        }
        //Для выполнения в основном потоке
        public void MyInvoke(Action action)
        {
            Invoke(action);
        }
    }
}

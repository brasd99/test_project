﻿using System;
using System.Windows.Forms;

namespace Тестовое_Задание
{
    public partial class filename_creator : Form
    {
        private TextBox textbox;

        public filename_creator(TextBox textbox)
        {

            InitializeComponent();
            //Ссылка на текстбокс основной формы
            this.textbox = textbox;

        }
        //Перемещение формы
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool ReleaseCapture();
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }


        }
        private void label1_MouseDown(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
    
          
        
        private void button1_Click(object sender, EventArgs e)
        {
          
                textBox1.Text += "?";
           
        }
        private void button2_Click(object sender, EventArgs e)
        {
          
                textBox1.Text += "*";
          
        }
        //Проверка на введённую маску
        private void button3_Click(object sender, EventArgs e)
        {

                    textbox.Text = textBox1.Text;
                    this.Close();

          
        }
        //Выход
        private void button_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
           
                textBox1.Text += ".";
          
        }
        //Удаление символов
        private void button5_Click(object sender, EventArgs e)
        {
          
                if (!String.IsNullOrEmpty(textBox1.Text))
                    textBox1.Text = textBox1.Text.Remove(textBox1.Text.Length - 1, 1);
         
        }
        //Ввод точки
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            { 
                if ((textBox1.Text.Split('.').Length - 1 > 0)) button4.Enabled = false;
                else button4.Enabled = true;
            }
           
        }
    }
}

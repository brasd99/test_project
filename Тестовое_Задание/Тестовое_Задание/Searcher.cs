﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Тестовое_Задание
{
    class Searcher
    {
        private Form1 form1;
        private int files_count;
        private int true_files_count;
        Regex mask;
        public Searcher(Form1 form1)
        {
                //Ссылка на основную форму
            this.form1 = form1;
            files_count = 0;
            true_files_count = 0;
            if (String.IsNullOrEmpty(form1.filename))
            {
                mask = new Regex(".*[.].*");
            }
            else
                mask = new Regex(form1.filename.Replace(".", "[.]").Replace("*", ".*").Replace("?", "."));
            GetEverything(form1.start_directory, form1.filename, form1.text);
                UpdateInfo("Поиск окончен", files_count,true_files_count);
                form1.timer1.Stop();
        }
        //Получаем файлы по указанным критериям
         private void GetEverything(string start_directory, string filename, string text)
         {
                string[] files = Directory.GetFiles(start_directory);
                foreach (string file in files)
                {
                form1.manualResetEvent.WaitOne();
                files_count++;
                //Используем маску
                if (mask.IsMatch(new DirectoryInfo(file).Name))
                {
                    //Проверяем содержание файла
                    if (File.ReadAllText(file).Contains(text))
                    {
                        true_files_count++;
                        //Обновляем дерево
                        BuildTree(form1.treeView_result.Nodes, file);
                        Action action = () =>
                        form1.treeView_result.Nodes[0].ExpandAll();
                        form1.MyInvoke(action);
                    }
                }
                UpdateInfo(new DirectoryInfo(file).Name, files_count,true_files_count);
            }
                //Проверяем каждую директорию
                string[] directories = Directory.GetDirectories(start_directory);
                foreach (string directory in directories)
                {
                    //Рекурсия
                    GetEverything(directory, filename, text);
                }
           

        }
        //Статус выполнения задачи
         private void UpdateInfo(string Filename,int File_Count,int True_Files_Count)
        {
            Action action = () =>
                form1.label_status.Text = "Статус: прошло: " + Get_Timer(form1.Timer_Tick) + ", Проверка файла:" + Filename + ",\r\nВсего обработано:" + File_Count + " файлов (найдено: "+True_Files_Count+" файлов)";
            form1.MyInvoke(action);

        }
        private string Get_Timer(int Timer_Tick)
        {
            string result;
            if(Timer_Tick<60)
            return Timer_Tick.ToString()+" сек";
            else
            {
                if (Timer_Tick / 60 < 60)
                {
                    result=Timer_Tick/60+" мин"+Timer_Tick%60+" сек";
                    return result;
                }
                else
                {
                    result = Timer_Tick / 60 + " ч" + Timer_Tick / 3600 + " мин"+Timer_Tick%Math.Pow(60,3)+" сек";
                    return result;
                }
            }
        }
        //Построение дерева по полному имени файла
        private void BuildTree(TreeNodeCollection nodes, string list)
        {
            string[] parts = list.Split('\\');
            var childs = nodes;
            for (int i = 0; i < parts.Length; i++)
            {
                Action action = () =>
                {
                    childs = FindOrCreateNode(childs, parts[i]).Nodes;
                };
                form1.MyInvoke(action);
            }  
        }
        //Правильное добавление, без дубликатов
        private TreeNode FindOrCreateNode(TreeNodeCollection coll, string name)
        {
            var found = coll.Find(name.ToLower(), false);
            if (found.Length > 0) return found[0];
            return coll.Add(name.ToLower(), name);
        }
     
    }
}

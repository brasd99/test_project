﻿namespace Тестовое_Задание
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBox_start_directory = new System.Windows.Forms.TextBox();
            this.groupBox_criter = new System.Windows.Forms.GroupBox();
            this.button_filename = new System.Windows.Forms.Button();
            this.button_choose_directory = new System.Windows.Forms.Button();
            this.button_stop = new System.Windows.Forms.Button();
            this.button_pause = new System.Windows.Forms.Button();
            this.button_start = new System.Windows.Forms.Button();
            this.label_filetext = new System.Windows.Forms.Label();
            this.textBox_filetext = new System.Windows.Forms.TextBox();
            this.label_filename = new System.Windows.Forms.Label();
            this.label_start_directory = new System.Windows.Forms.Label();
            this.textBox_filename = new System.Windows.Forms.TextBox();
            this.button_exit = new System.Windows.Forms.Button();
            this.panel_osn = new System.Windows.Forms.Panel();
            this.label_project_name = new System.Windows.Forms.Label();
            this.groupBox_result = new System.Windows.Forms.GroupBox();
            this.treeView_result = new System.Windows.Forms.TreeView();
            this.label_status = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox_criter.SuspendLayout();
            this.panel_osn.SuspendLayout();
            this.groupBox_result.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox_start_directory
            // 
            this.textBox_start_directory.Location = new System.Drawing.Point(6, 45);
            this.textBox_start_directory.Name = "textBox_start_directory";
            this.textBox_start_directory.Size = new System.Drawing.Size(258, 22);
            this.textBox_start_directory.TabIndex = 0;
            this.textBox_start_directory.Leave += new System.EventHandler(this.textBox_start_directory_Leave);
            // 
            // groupBox_criter
            // 
            this.groupBox_criter.Controls.Add(this.button_filename);
            this.groupBox_criter.Controls.Add(this.button_choose_directory);
            this.groupBox_criter.Controls.Add(this.button_stop);
            this.groupBox_criter.Controls.Add(this.button_pause);
            this.groupBox_criter.Controls.Add(this.button_start);
            this.groupBox_criter.Controls.Add(this.label_filetext);
            this.groupBox_criter.Controls.Add(this.textBox_filetext);
            this.groupBox_criter.Controls.Add(this.label_filename);
            this.groupBox_criter.Controls.Add(this.textBox_start_directory);
            this.groupBox_criter.Controls.Add(this.label_start_directory);
            this.groupBox_criter.Controls.Add(this.textBox_filename);
            this.groupBox_criter.Location = new System.Drawing.Point(12, 50);
            this.groupBox_criter.Name = "groupBox_criter";
            this.groupBox_criter.Size = new System.Drawing.Size(270, 396);
            this.groupBox_criter.TabIndex = 1;
            this.groupBox_criter.TabStop = false;
            this.groupBox_criter.Text = "Критерии поиска";
            // 
            // button_filename
            // 
            this.button_filename.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_filename.Font = new System.Drawing.Font("Microsoft Sans Serif", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_filename.Location = new System.Drawing.Point(218, 99);
            this.button_filename.Name = "button_filename";
            this.button_filename.Size = new System.Drawing.Size(45, 22);
            this.button_filename.TabIndex = 13;
            this.button_filename.Text = "🔍";
            this.button_filename.UseVisualStyleBackColor = true;
            this.button_filename.Click += new System.EventHandler(this.button_filename_Click);
            // 
            // button_choose_directory
            // 
            this.button_choose_directory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_choose_directory.Font = new System.Drawing.Font("Microsoft Sans Serif", 4.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_choose_directory.Location = new System.Drawing.Point(218, 45);
            this.button_choose_directory.Name = "button_choose_directory";
            this.button_choose_directory.Size = new System.Drawing.Size(45, 22);
            this.button_choose_directory.TabIndex = 12;
            this.button_choose_directory.Text = "🔍";
            this.button_choose_directory.UseVisualStyleBackColor = true;
            this.button_choose_directory.Click += new System.EventHandler(this.button_choose_directory_Click);
            // 
            // button_stop
            // 
            this.button_stop.Location = new System.Drawing.Point(9, 361);
            this.button_stop.Name = "button_stop";
            this.button_stop.Size = new System.Drawing.Size(255, 27);
            this.button_stop.TabIndex = 11;
            this.button_stop.Text = "Стоп";
            this.button_stop.UseVisualStyleBackColor = true;
            this.button_stop.Click += new System.EventHandler(this.button_stop_Click);
            // 
            // button_pause
            // 
            this.button_pause.Enabled = false;
            this.button_pause.Location = new System.Drawing.Point(9, 328);
            this.button_pause.Name = "button_pause";
            this.button_pause.Size = new System.Drawing.Size(255, 27);
            this.button_pause.TabIndex = 10;
            this.button_pause.Text = "Пауза";
            this.button_pause.UseVisualStyleBackColor = true;
            this.button_pause.Click += new System.EventHandler(this.button_pause_Click);
            // 
            // button_start
            // 
            this.button_start.Location = new System.Drawing.Point(9, 295);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(255, 27);
            this.button_start.TabIndex = 9;
            this.button_start.Text = "Старт";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // label_filetext
            // 
            this.label_filetext.AutoSize = true;
            this.label_filetext.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_filetext.Location = new System.Drawing.Point(6, 133);
            this.label_filetext.Name = "label_filetext";
            this.label_filetext.Size = new System.Drawing.Size(210, 17);
            this.label_filetext.TabIndex = 8;
            this.label_filetext.Text = "Текст, содержащийся в файле";
            // 
            // textBox_filetext
            // 
            this.textBox_filetext.Location = new System.Drawing.Point(6, 153);
            this.textBox_filetext.Multiline = true;
            this.textBox_filetext.Name = "textBox_filetext";
            this.textBox_filetext.Size = new System.Drawing.Size(258, 135);
            this.textBox_filetext.TabIndex = 7;
            this.textBox_filetext.Leave += new System.EventHandler(this.textBox_filetext_Leave);
            // 
            // label_filename
            // 
            this.label_filename.AutoSize = true;
            this.label_filename.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_filename.Location = new System.Drawing.Point(3, 79);
            this.label_filename.Name = "label_filename";
            this.label_filename.Size = new System.Drawing.Size(151, 17);
            this.label_filename.TabIndex = 6;
            this.label_filename.Text = "Шаблон имени файла";
            // 
            // label_start_directory
            // 
            this.label_start_directory.AutoSize = true;
            this.label_start_directory.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_start_directory.Location = new System.Drawing.Point(3, 25);
            this.label_start_directory.Name = "label_start_directory";
            this.label_start_directory.Size = new System.Drawing.Size(160, 17);
            this.label_start_directory.TabIndex = 4;
            this.label_start_directory.Text = "Стартовая директория";
            // 
            // textBox_filename
            // 
            this.textBox_filename.Enabled = false;
            this.textBox_filename.Location = new System.Drawing.Point(6, 99);
            this.textBox_filename.Name = "textBox_filename";
            this.textBox_filename.Size = new System.Drawing.Size(258, 22);
            this.textBox_filename.TabIndex = 5;
            this.textBox_filename.TextChanged += new System.EventHandler(this.textBox_filename_TextChanged);
            // 
            // button_exit
            // 
            this.button_exit.BackColor = System.Drawing.Color.Red;
            this.button_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_exit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button_exit.Location = new System.Drawing.Point(912, 0);
            this.button_exit.Name = "button_exit";
            this.button_exit.Size = new System.Drawing.Size(75, 32);
            this.button_exit.TabIndex = 2;
            this.button_exit.Text = "Выход";
            this.button_exit.UseVisualStyleBackColor = false;
            this.button_exit.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel_osn
            // 
            this.panel_osn.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel_osn.Controls.Add(this.label_project_name);
            this.panel_osn.Controls.Add(this.button_exit);
            this.panel_osn.Location = new System.Drawing.Point(12, 12);
            this.panel_osn.Name = "panel_osn";
            this.panel_osn.Size = new System.Drawing.Size(987, 32);
            this.panel_osn.TabIndex = 3;
            this.panel_osn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_osn_MouseDown);
            // 
            // label_project_name
            // 
            this.label_project_name.AutoSize = true;
            this.label_project_name.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label_project_name.Location = new System.Drawing.Point(3, 8);
            this.label_project_name.Name = "label_project_name";
            this.label_project_name.Size = new System.Drawing.Size(97, 17);
            this.label_project_name.TabIndex = 3;
            this.label_project_name.Text = "Имя_проекта";
            this.label_project_name.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label_project_name_MouseDown);
            // 
            // groupBox_result
            // 
            this.groupBox_result.Controls.Add(this.treeView_result);
            this.groupBox_result.Location = new System.Drawing.Point(289, 50);
            this.groupBox_result.Name = "groupBox_result";
            this.groupBox_result.Size = new System.Drawing.Size(710, 396);
            this.groupBox_result.TabIndex = 4;
            this.groupBox_result.TabStop = false;
            this.groupBox_result.Text = "Результат выполнения";
            // 
            // treeView_result
            // 
            this.treeView_result.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView_result.Location = new System.Drawing.Point(3, 18);
            this.treeView_result.Name = "treeView_result";
            this.treeView_result.Size = new System.Drawing.Size(704, 375);
            this.treeView_result.TabIndex = 0;
            // 
            // label_status
            // 
            this.label_status.AutoSize = true;
            this.label_status.Location = new System.Drawing.Point(21, 453);
            this.label_status.Name = "label_status";
            this.label_status.Size = new System.Drawing.Size(57, 17);
            this.label_status.TabIndex = 5;
            this.label_status.Text = "Статус:";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1011, 479);
            this.Controls.Add(this.label_status);
            this.Controls.Add(this.groupBox_result);
            this.Controls.Add(this.panel_osn);
            this.Controls.Add(this.groupBox_criter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox_criter.ResumeLayout(false);
            this.groupBox_criter.PerformLayout();
            this.panel_osn.ResumeLayout(false);
            this.panel_osn.PerformLayout();
            this.groupBox_result.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_start_directory;
        private System.Windows.Forms.GroupBox groupBox_criter;
        private System.Windows.Forms.Button button_exit;
        private System.Windows.Forms.Panel panel_osn;
        private System.Windows.Forms.Label label_project_name;
        private System.Windows.Forms.Button button_stop;
        private System.Windows.Forms.Button button_pause;
        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.Label label_filetext;
        private System.Windows.Forms.TextBox textBox_filetext;
        private System.Windows.Forms.Label label_filename;
        private System.Windows.Forms.Label label_start_directory;
        private System.Windows.Forms.TextBox textBox_filename;
        private System.Windows.Forms.GroupBox groupBox_result;
        public System.Windows.Forms.Label label_status;
        public System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button_filename;
        private System.Windows.Forms.Button button_choose_directory;
        public System.Windows.Forms.TreeView treeView_result;
    }
}

